1. Як можна оголосити змінну у Javascript?
У JavaScript змінні можна оголосити за допомогою ключового слова let, const або var. Наприклад, щоб оголосити змінну з іменем x і присвоїти їй значення 5, можна написати let x = 5. Якщо використати let, змінна може бути переназначена пізніше у програмі. Якщо використати const, то змінна стає константою і її значення не можна змінити. Ключове слово var також можна використовувати для оголошення змінних, але воно має певні особливості в порівнянні з let та const, тому рекомендується використовувати let або const в сучасному JavaScript.

2. У чому різниця між функцією prompt та функцією confirm?
Основна відмінність між функцією prompt та функцією confirm полягає у тому, що функція prompt дозволяє відобразити діалогове вікно з текстовим полем для введення даних та повертає те, що користувач написав у цьому полі, тоді як confirm повертає true, якщо користувач натиснув "Так", або false, якщо користувач натиснув "Ні".

3. Що таке неявне перетворення типів? Наведіть один приклад.
В JavaScript неявне перетворення типів відбувається, коли оператор або функція працює зі значеннями різних типів, але очікується значення лише одного типу. У цьому випадку JavaScript автоматично перетворює значення одного типу на інший, щоб виконати потрібну операцію.
let x = 10;
let y = "20";
let z = x + y;
console.log(z); // "1020"